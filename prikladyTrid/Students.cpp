#include <iostream>
using namespace std;

class Student{
public:
    string m_jmeno;
    int m_rokStudia;
    string m_obor;
    bool m_stipendium;

    void setJmeno(string jmeno){
        m_jmeno = jmeno;
    }
    string getJmeno(){
        return m_jmeno;
    }
    void setRokStudia(int rokStudia){
        m_rokStudia = rokStudia;
    }
    int getRokStudia(){
        return m_rokStudia;
    }
    void setObor(string obor){
        m_obor = obor;
    }
    string getObor(){
        return m_obor;
    }
    void setStipendium(bool stipendium){
        m_stipendium = stipendium;
    }
    bool getStipendium(){
    return m_stipendium;
}
    void printInfo(){
        cout << "Jmeno studenta je: " << m_jmeno << endl;
        cout << "Rok studia je: " << m_rokStudia << endl;
        cout << "Obor je: " << m_obor << endl;
        cout << "Stipendium: " << m_stipendium << endl << endl;
    }
};

int main() {
    Student* mark;
    mark = new Student;
    mark->setJmeno("Mark");
    mark->setRokStudia(2);
    mark->setObor("Ekonomika");
    mark->setStipendium(true);
    mark->printInfo();
}
