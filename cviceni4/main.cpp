#include <iostream>
using namespace std;

class Passenger{
    string m_name;
    float m_weight;
    float m_luggageWeight;
public:
    Passenger(string name, float weight, float luggageWeight){
        m_name = name;
        m_weight = weight;
        m_luggageWeight = luggageWeight;
    }
    Passenger(string name, float luggageWeight){
        m_name = name;
        m_luggageWeight = luggageWeight;
        m_weight = 70.0;
    }
  float getTotalWeight(){
        return m_weight + m_luggageWeight;
    }
    string getName(){
        return m_name;
    }
};
class Elevator{
    float m_maxWeight;
    float m_currentWeight;
    int m_passengers;
public:
    Elevator(){
        m_maxWeight = 200.0;
        m_currentWeight = 0.0;
        m_passengers = 0;
    }
    void addPassenger(Passenger* newPassenger){
        float newWeight = m_currentWeight + newPassenger->getTotalWeight();
        if(newWeight <= m_maxWeight){
            m_currentWeight = newWeight;
            m_passengers += 1;
        } else{
            cout << "Pasaziera " << newPassenger->getName() << "nie je mozne uz pridat!!!" << endl;

        }
    }
    void printInfo(){
        cout << "Akt vaha: " << m_currentWeight << endl;
        cout << "Prime vaha: " << calculateAverage() << endl;
        cout << "Pocet pas: " << m_passengers << endl;
    }
private:
    float calculateAverage(){
        return m_currentWeight / m_passengers;
    }
};
int main() {


}
