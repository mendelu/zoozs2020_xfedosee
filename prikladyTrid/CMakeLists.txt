cmake_minimum_required(VERSION 3.17)
project(prikladyTrid)

set(CMAKE_CXX_STANDARD 14)

add_executable(prikladyTrid Students.cpp)