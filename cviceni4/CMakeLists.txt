cmake_minimum_required(VERSION 3.17)
project(cviceni4)

set(CMAKE_CXX_STANDARD 14)

add_executable(cviceni4 main.cpp)