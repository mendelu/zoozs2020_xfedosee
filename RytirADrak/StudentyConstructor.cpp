#include <iostream>
using namespace std;

class Student{
public:
    string m_jmeno;
    int m_rodneCislo;
    string m_adresaBydliste;

    Student(string jmeno, int rodneCislo){
        m_jmeno = jmeno;
        m_rodneCislo = rodneCislo;
    }
    string getJmeno(){
        return m_jmeno;
    }
    int getRodneCislo(){
        return m_rodneCislo;
    }
    void setAdresa(string adresaBydliste){
        m_adresaBydliste = adresaBydliste;
    }
    string getAdresa(){
        return m_adresaBydliste;
    }
    void printInfo(){
        cout << "Jmeno: " << m_jmeno << endl;
        cout << "Rodne cislo: " << m_rodneCislo << endl;
        cout << "Adresa bydliste: " << m_adresaBydliste << endl << endl;
    }
};

int main() {
    Student* mark = new Student("Mark", 26061999);
    mark->setAdresa("Brno, Cejl 469/71");
    mark->printInfo();
    Student* chris = new Student("Chris", 17012000);
    chris->setAdresa("Hzbesova 17");
    chris->printInfo();
}
