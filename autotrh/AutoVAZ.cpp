#include <iostream>
using namespace std;

class Auto{
public:
    string m_spz;
    float m_pocetKm;
    string m_predchoziMajitel;

    Auto(string spz, float pocetKm, string predchoziMajitel){
        m_spz = spz;
        m_pocetKm = pocetKm;
        m_predchoziMajitel = predchoziMajitel;
    }
    string getSpz(){
        return m_spz;
    }
    float getPocetKm(){
        return m_pocetKm;
    }
    string getPredchoziMajitel(){
        return m_predchoziMajitel;
    }
    void printInfo(){
        cout << "SPZ: " << m_spz << endl;
        cout << "Najeto km: " << m_pocetKm << endl;
        cout << "Predchozi majitel: " << m_predchoziMajitel << endl;
    }
};
int main() {
    Auto* VAZ2105 = new Auto("Y345BO", 60000, "Roma");
    VAZ2105->printInfo();
    Auto* VAZ2112 = new Auto("E333EE", 66.6, "Pasha Volk");
    VAZ2112->printInfo();
    Auto* VAZ2131 = new Auto("H192UJ", 198374, "Den");
    VAZ2131->printInfo();
}
